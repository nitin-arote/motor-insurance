import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { TwoWheelerComponent } from "./Home/two-wheeler/two-wheeler.component";
// import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
// import {MotorComponent  } from "./Home/motor/motor.component";
// import {TwoWheelerHomeComponent} from './Home/two-wheeler-home/two-wheeler-home.component'
// import { TwoWheelerPlansComponent } from "./Home/two-wheeler-plans/two-wheeler-plans.component";
// import { FourWheelerGetquoteComponent } from "./Car/four-wheeler-getquote/four-wheeler-getquote.component";
// import { FourWheelerGetDetailsComponent } from "./Car/four-wheeler-get-details/four-wheeler-get-details.component";
// import { FourWheelerPlansComponent } from './Car/four-wheeler-plans/four-wheeler-plans.component';
// import { GetMotorDeatilsComponent } from "./get-motor-deatils/get-motor-deatils.component";
const routes: Routes = [
  // {
  //   path:'', pathMatch:"full",redirectTo:"motor",
  // },
  // {path:'two-wheeler',component:TwoWheelerHomeComponent},
  // { path:'motor',component:MotorComponent},
  // {path:'two-wheeler-plans',component:TwoWheelerPlansComponent},
  // //
  // {path:'four-wheeler-get-quote',component:FourWheelerGetquoteComponent},
  // {path:'four-wheeler-get-details',component:FourWheelerGetDetailsComponent},
  // {path:'four-wheeler-plans',component:FourWheelerPlansComponent},
  // {path:'get-motor-deatils',component:GetMotorDeatilsComponent},
  // {path:'**',component:PageNotFoundComponent},
   {
    path: '',
    redirectTo: 'four-wheeler',
    pathMatch: 'full'
  },
  {
    path: 'four-wheeler',
    loadChildren: () =>
      import('./four-wheeler/four-wheeler.module').then(m => m.FourWheelerModule)
  },
  {
    path: 'two-wheeler',
    loadChildren: () =>
      import('./two-wheeler/two-wheeler.module').then(m => m.TwoWheelerModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
