import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from "./layer/footer/footer.component";
import { HeaderComponent } from "./layer/header/header.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { SvgModule } from "./svg/svg.module";
import { TwoWheelerComponent } from './two-wheeler/two-wheeler/two-wheeler.component';
// import { MotorComponent } from './Home/motor/motor.component';
import { TwoWheelerHomeComponent } from './two-wheeler/two-wheeler-home/two-wheeler-home.component';
import { FormsModule,ReactiveFormsModule} from "@angular/forms";
// import { TwoWheelerPlansComponent } from "./two-wheeler/two-wheeler-plans/two-wheeler-plans.component";
import { SharedModule as localSharedModule } from "./shared/shared.module";
import { MyDatePickerModule } from 'mydatepicker';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { GetMotorDeatilsComponent } from './get-motor-deatils/get-motor-deatils.component';
@NgModule({
  declarations: [
    AppComponent,FooterComponent,HeaderComponent,PageNotFoundComponent, GetMotorDeatilsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SvgModule,FormsModule,ReactiveFormsModule,HttpClientModule,localSharedModule,MyDatePickerModule,AngularMultiSelectModule,DropDownListModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports:[SvgModule,MyDatePickerModule]
})
export class AppModule { }
