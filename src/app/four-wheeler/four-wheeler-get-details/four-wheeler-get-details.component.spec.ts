import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FourWheelerGetDetailsComponent } from './four-wheeler-get-details.component';

describe('FourWheelerGetDetailsComponent', () => {
  let component: FourWheelerGetDetailsComponent;
  let fixture: ComponentFixture<FourWheelerGetDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FourWheelerGetDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FourWheelerGetDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
