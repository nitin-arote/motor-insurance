import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { FourWheelerService } from "../four-wheeler.service";
import { CommonService } from "../../shared/common.service";
@Component({
  selector: 'app-four-wheeler-get-details',
  templateUrl: './four-wheeler-get-details.component.html',
  styleUrls: ['./four-wheeler-get-details.component.css']
})
export class FourWheelerGetDetailsComponent implements OnInit {

  constructor(private fourWheelerService: FourWheelerService, private formBuilder: FormBuilder, private commonService: CommonService, private router: Router) { }

  ngOnInit(): void {

    this.checkAvailbleData()
    this.addYear();
    // this.getAllCity();
    // this.getAllManufacturer();
    // this.getAllCompaines();
  }


  fuelTypeArr = [
    { title: 'Petrol' }, { title: 'CNG' }, { title: 'Diesel' }, { title: 'Petrol' }
  ]
  // carVarientArr = [
  //   { title: 'E 1.2 Petrol (1197 cc)' }, { title: 'S 1.2 AMT Petrol (1197 cc)' }, { title: 'S 1.2 Petrol (1197 cc)' }, { title: 'SX 1.2 (O) Petrol (1197 cc)' }, { title: 'SX 1.2 Petrol (1197 cc)' }, { title: 'SX Plus 1.0 Petrol (998 cc)' }, { title: 'SX Plus 1.2 AMT Petrol (1197 cc)' }
  // ];

  /* Variables */
  carYear = [{ year: 'Brand new car' }]
  carCity = '';
  carBrand = '';
  carModel = '';
  carVarient = '';
  carFuelType = '';
  carRegistrationYear = ''
  quoteMode = 1;
  searchFlag = false;
  isLoading: boolean = true
  searchField: any = '';
  searchFieldBrand: any = '';
  searchFieldModel: any = '';
  searchFieldVariant: any = '';

  /* Objects */
  carDetailsObj = {
    carCity: '',
    carBrand: '',
    carModel: '',
    carVarient: '',
    carFuelType: '',
    carRegistrationYear: '',
    quoteMode: 1,
    clientName: '',
    clientEmail: '',
    clientMobile: '',
    registrationDate :''
  }

  /* Arrays */

  cityArr = [];
  carCityArrBK: any = [];
  allVehicaleData: any = [];
  allVehicaleDataBK: any = [];
  carModals: any = [];
  carModalsBK: any = []
  carVarientArr: any = [];
  carVarientArrBK: any = []

  /* forms */
  carDetailsForm = this.formBuilder.group({
    clientName: new FormControl(null, Validators.required),
    clientEmail: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
    clientMobile: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
    registrationDate :new FormControl(null, Validators.required),
  });
  get f() {
    return this.carDetailsForm.controls;
  }



  addYear() {
    for (let i = 2019; i >= 1995; i--) {
      this.carYear.push({ year: '' + i })
    }
  }

  checkAvailbleData(){
    if(this.fourWheelerService.carManufacturer.length ==0){
      localStorage.removeItem('c_details')
    }
    (this.fourWheelerService.allCity.length == 0) ? this.getAllCity() : (this.cityArr = this.carCityArrBK = this.fourWheelerService.allCity) ;
    (this.fourWheelerService.carManufacturer.length == 0) ? this.getAllManufacturer() : (this.allVehicaleData = this.allVehicaleData = this.fourWheelerService.carManufacturer) ;
    let  data =JSON.parse( localStorage.getItem('c_details'));
    if(data != null || data != undefined){
      this.carDetailsForm.controls['clientName'].setValue(data['clientName']);
      this.carDetailsForm.controls['clientEmail'].setValue(data['clientEmail'])
      this.carDetailsForm.controls['clientMobile'].setValue(data['clientMobile']);
      this.carDetailsForm.controls['registrationDate'].setValue(data['registrationDate']);

      this.carCity = data['carCity'] != '' ? data['carCity'] : '';
      // this.carYear = data['carRegistrationYear'] != '' ? data['carRegistrationYear'] : '';
      this.carBrand = data['carBrand'] != '' ? data['carBrand'] : '';
      this.carModel = data['carModel'] != '' ? data['carModel'] : '';
      this.carVarient = data['carVarient'] != '' ? data['carVarient'] : '';
      this.carRegistrationYear = data['carRegistrationYear'] != '' ? data['carRegistrationYear'] : '';
      this.carDetailsObj = data
      this.carModals = this.carModalsBK = this.fourWheelerService.carModel;
      this.carVarientArr = this.carVarientArrBK = this.fourWheelerService.carVariant;
    }

    this.sortArray()
  }

  getAllCity() {
    let data = {
      option: 'fetch-all-rto-city'
    }
    this.fourWheelerService.getAllCity(data).subscribe(
      response => {
        console.log("fetch-all-rto-city ===>>>>", response['data']);
        this.cityArr = response['data'];
        this.carCityArrBK = response['data'];
        this.fourWheelerService.allCity =  response['data'];
      },
      error => {
        console.error('==', error)
      },
      () => {
        this.isLoading = false
      }
    );
  }
  // getAllModels
  getAllManufacturer() {
    // this.isLoading = true;
    let data = {
      option: 'fetch-all-distinct-manufacturer'
    }
    this.fourWheelerService.getAllCity(data).subscribe(
      response => {
        console.log("fetch-all-distinct-manufacturer ====>>>>", response['data']);
        this.allVehicaleData = response['data'];
        this.allVehicaleDataBK = response['data'];
        this.fourWheelerService.carManufacturer =  response['data'];
      },
      error => {
        console.error('==', error)
      },
      () => {
        // this.isLoading = false
      }
    );
  }
  getAllModels(manufacturer) {
    this.isLoading = true;
    let data = {
      option: 'fetch-all-distinct-model',
      // modelcode:'',
      manufacturercode: manufacturer
    }
    this.fourWheelerService.getAllCity(data).subscribe(
      response => {
        console.log("fetch-all-distinct-model ====>>>>", response['data']);
        this.carModals = response['data'];
        this.carModalsBK = response['data'];
        this.fourWheelerService.carModel =  response['data'];
      },
      error => {
        console.error('==', error)
      },
      () => {
        this.isLoading = false
      }
    );
  }
  getAllVarient(manufacturer, varient) {
    this.isLoading = true;
    let data = {
      option: 'fetch-all-distinct-varient',
      manufacturercode: this.carBrand,
      modelcode: varient
    }
    this.fourWheelerService.getAllCity(data).subscribe(
      response => {
        console.log("fetch-all-distinct-varient ====>>>>", response);
        this.carVarientArr = response['data'];
        this.carVarientArrBK = response['data'];
        this.fourWheelerService.carVariant =  response['data'];
      },
      error => {
        console.error('==', error)
      },
      () => {
        this.isLoading = false
      }
    );
  }

  getAllCompaines() {
    let data = {
      option: 'fetch-all-manufacturer-model-varient',
      // meufacturedcode : maruticoj
      // meufacturedcode : maruticoj
    }
    // this.fourWheelerService.getAllCity(data).subscribe(
    //   response => {
    //     console.log("fetch-all-previous-insurance-company =====================+>",response['data']);
    //     // this.cityArr = response['data'];
    //     // this.carCityArrBK = response['data'];
    //   },
    //   error => {
    //     console.error('==',error)
    //   }
    // );
  }


  addCity(data, index) {
    console.log('carCity====================>>>', data);
    this.carCity = data['NUM_REGISTRATION_CODE'];
    this.carDetailsObj['carCity'] = this.carCity;
  }

  addManufacturer(data, index) {
    console.log('MANUFACTURER====================>>>', data)
    this.carBrand = data['MANUFACTURER'];
    this.carDetailsObj['carBrand'] = data['MANUFACTURER'];
    this.getAllModels(data['MANUFACTURER'])
  }

  addCarModals(data, index) {
    console.log('MODEL====================>>>', data)
    this.carModel = data['MODEL'];
    this.carDetailsObj['carModel'] = this.carModel;
    this.getAllVarient(this.carDetailsObj['carBrand'], data['MODEL'])
  }

  addCarVarient(data) {
    console.log('carVarient====================>>>', data)
    this.carVarient = data['VARIANT'];
    this.carDetailsObj['carVarient'] = data['VARIANT'];
    this.carDetailsObj['carFuelType'] = data['FUEL'];
    this.carFuelType = data['FUEL'];
  }

  addCarFuelType(data) {
    console.log('carFuelType====================>>>', data)
    this.carFuelType = data['title'];
    this.carDetailsObj['carFuelType'] = this.carFuelType;
  }

  addCarYear(data, index) {
    console.log('====================>>>', data)
    this.carRegistrationYear = data['year'];
    this.carDetailsObj['carRegistrationYear'] = data['year'];
  }
  searchPolicy = function (iEvent) {
    // if (iEvent && (iEvent.eventType == 'click' || iEvent.iEvent.keyCode == 13)) {
    if (this.searchField == undefined) {
      return false
    }
    this.searchField = this.searchField.trim();
    if (this.searchField != "") {
      let data = this.carCityArrBK
      this.cityArr = [];
      for (let city of data) {
        if (city['NUM_REGISTRATION_CODE'].toLowerCase().includes(this.searchField.toLowerCase()) || city['TXT_RTO_LOCATION_DESC'].toLowerCase().includes(this.searchField.toLowerCase())) {
          this.cityArr.push(city)
          //console.log('----',this.policyData)
        }
      }
      //console.log('inside if',_obj)
      this.searchFlag = true;
    } else {
      this.cityArr = this.carCityArrBK
      this.searchFlag = false;
    }
  }
  searchCarBrand = function (iEvent) {
    // if (iEvent && (iEvent.eventType == 'click' || iEvent.iEvent.keyCode == 13)) {
    if (this.searchFieldBrand == undefined) {
      return false
    }
    // allVehicaleData :any = [];
    // allVehicaleDataBK :any = []
    this.searchFieldBrand = this.searchFieldBrand.trim();
    if (this.searchFieldBrand != "") {
      let data = this.allVehicaleDataBK
      this.allVehicaleData = [];
      for (let brand of data) {
        if (brand['MANUFACTURER'].toLowerCase().includes(this.searchFieldBrand.toLowerCase())) {
          this.allVehicaleData.push(brand)
          //console.log('----',this.policyData)
        }
      }
      //console.log('inside if',_obj)
      this.searchFlag = true;
    } else {
      this.allVehicaleData = this.allVehicaleDataBK
      this.searchFlag = false;
    }
  }
  searchModel = function (iEvent) {
    // if (iEvent && (iEvent.eventType == 'click' || iEvent.iEvent.keyCode == 13)) {
    if (this.searchFieldModel == undefined) {
      return false
    }
    // allVehicaleData :any = [];
    // allVehicaleDataBK :any = []
    this.searchFieldModel = this.searchFieldModel.trim();
    if (this.searchFieldModel != "") {
      let data = this.carModalsBK
      this.carModals = [];
      for (let model of data) {
        if (model['MODEL'].toLowerCase().includes(this.searchFieldModel.toLowerCase())) {
          this.carModals.push(model)
          //console.log('----',this.policyData)
        }
      }
      //console.log('inside if',_obj)
      this.searchFlag = true;
    } else {
      this.carModals = this.carModalsBK
      this.searchFlag = false;
    }
  }
  searchVariant = function (iEvent) {
    // if (iEvent && (iEvent.eventType == 'click' || iEvent.iEvent.keyCode == 13)) {
    if (this.searchFieldVariant == undefined) {
      return false
    }
    this.searchFieldVariant = this.searchFieldVariant.trim();
    if (this.searchFieldVariant != "") {
      let data = this.carVarientArrBK
      this.carVarientArr = [];
      for (let varient of data) {
        if (varient['VARIANT'].toLowerCase().includes(this.searchFieldVariant.toLowerCase())) {
          this.carVarientArr.push(varient)
        }
      }
      this.searchFlag = true;
    } else {
      this.carVarientArr = this.carVarientArrBK
      this.searchFlag = false;
    }
  }

  //
  getUserDetails(evt) {
    evt && evt.stopPropagation();
    console.error("====>>>>",this.carDetailsForm)
    this.carDetailsObj['registrationDate'] = this.carDetailsForm.controls['registrationDate'].value;
    this.carDetailsObj['clientName'] = this.carDetailsForm.controls['clientName'].value;
    this.carDetailsObj['clientEmail'] = this.carDetailsForm.controls['clientEmail'].value;
    this.carDetailsObj['clientMobile'] = this.carDetailsForm.controls['clientMobile'].value;
    localStorage.setItem('c_details', JSON.stringify(this.carDetailsObj));
    this.router.navigateByUrl('/four-wheeler/four-wheeler-plans');
  }


  /* */

  sortArray(){
    let obj = JSON.parse(localStorage.getItem('c_details'))
    for(let i in this.cityArr){
      // console.log('====>>>',i);
      if(obj['carCity'] == this.cityArr[i]['NUM_REGISTRATION_CODE']){
        let val = this.cityArr[i]
        this.cityArr.splice(+i,1);
        console.log("value",val)
        this.cityArr.unshift(val)
      }
    }

    for(let i in this.allVehicaleData){
      // console.log('====>>>',i);
      if(obj['carBrand'] == this.allVehicaleData[i]['MANUFACTURER']){
        let val = this.allVehicaleData[i]
        this.allVehicaleData.splice(+i,1);
        console.log("value",val)
        this.allVehicaleData.unshift(val)
      }
    }
    for(let i in this.carModals){
      // console.log('====>>>',i);
      if(obj['carModel'] == this.carModals[i]['MODEL']){
        let val = this.carModals[i]
        this.carModals.splice(+i,1);
        console.log("value",val)
        this.carModals.unshift(val)
      }
    }
    for(let i in this.carVarientArr){
      // console.log('====>>>',i);
      if(obj['carVarient'] == this.carVarientArr[i]['VARIANT']){
        let val = this.carVarientArr[i]
        this.carVarientArr.splice(+i,1);
        console.log("value",val)
        this.carVarientArr.unshift(val)
      }
    }
  }
  /* Select data div*/
  selectDiv(index) {
    this.quoteMode = index;
  };

  ngOnDestroy() {
    localStorage.setItem('c_details', JSON.stringify(this.carDetailsObj))
  }
}
