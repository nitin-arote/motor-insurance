import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FourWheelerGetquoteComponent } from './four-wheeler-getquote.component';

describe('FourWheelerGetquoteComponent', () => {
  let component: FourWheelerGetquoteComponent;
  let fixture: ComponentFixture<FourWheelerGetquoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FourWheelerGetquoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FourWheelerGetquoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
