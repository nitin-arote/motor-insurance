import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../shared/common.service";
import { FourWheelerService } from "../four-wheeler.service";
@Component({
  selector: 'app-four-wheeler-getquote',
  templateUrl: './four-wheeler-getquote.component.html',
  styleUrls: ['./four-wheeler-getquote.component.css']
})
export class FourWheelerGetquoteComponent implements OnInit {

  constructor(public commonService: CommonService, private fourWheelerService: FourWheelerService) { }

  carNumber: any = '';
  /* Objects */
  carDetailsObj = {
    carCity: '',
    carBrand: '',
    carModel: '',
    carVarient: '',
    carFuelType: '',
    carRegistrationYear: '',
    quoteMode: 1,
    clientName: '',
    clientEmail: '',
    clientMobile: '',
    registrationDate: '',
    insurance_upto : '',
    registration_date:'registration_date'
  }

  ngOnInit() { }

  getQuote() {
    console.log(this.carNumber)
    let data = this.carNumber;
    data = data.replace(/-/g, '');
    this.fourWheelerService.getCarDetails(data).subscribe(
      res => {
        console.log(res);
        /* here is not all data not found */
        this.carDetailsObj.carFuelType = res['fuel_type'];
        this.carDetailsObj.insurance_upto = res['insurance_upto'] 
        this.carDetailsObj.carBrand = res['make'] 
        this.carDetailsObj.clientName = res['owner_name'] 
        this.carDetailsObj.registration_date = res['registration_date'] 
        this.carDetailsObj.carFuelType = res['fuel_type'] 
      },
      err => {
        console.error(err)
      }
    )
  }
}
