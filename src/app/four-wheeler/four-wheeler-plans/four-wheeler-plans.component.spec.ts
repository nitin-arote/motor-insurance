import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FourWheelerPlansComponent } from './four-wheeler-plans.component';

describe('FourWheelerPlansComponent', () => {
  let component: FourWheelerPlansComponent;
  let fixture: ComponentFixture<FourWheelerPlansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FourWheelerPlansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FourWheelerPlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
