import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { IMyDpOptions } from 'mydatepicker';
import { FourWheelerService } from "../four-wheeler.service";
import { CommonService } from "../../shared/common.service";
@Component({
  selector: 'app-four-wheeler-plans',
  templateUrl: './four-wheeler-plans.component.html',
  styleUrls: ['./four-wheeler-plans.component.css']
})
export class FourWheelerPlansComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private router: Router, private fourWheelerService: FourWheelerService,public commonService : CommonService) { }

  isLoading: boolean = false;
  policyList = [
    { policy_id: "1", name: "Home Insurance", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.", start_date: "20/08/2020", end_date: "20/08/2020" },
    { policy_id: "2", name: "Car Insurance", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.", start_date: "20/08/2020", end_date: "20/08/2020" },
    { policy_id: "3", name: "Two Wheelar Insurance", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.", start_date: "20/08/2020", end_date: "20/08/2020" },
    { policy_id: "4", name: "Health Insurance", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.", start_date: "20/08/2020", end_date: "20/08/2020" },
    { policy_id: "5", name: "Health Insurance", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.", start_date: "20/08/2020", end_date: "20/08/2020" },
    { policy_id: "6", name: "Corona Kavach", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.", start_date: "20/08/2020", end_date: "20/08/2020" },
    { policy_id: "7", name: "Home Insurance", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.", start_date: "20/08/2020", end_date: "20/08/2020" },
    { policy_id: "8", name: "Home Insurance", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.", start_date: "20/08/2020", end_date: "20/08/2020" }
  ]
  varientArr :any= this.fourWheelerService.carVariant
  allPolicyList = this.policyList;
  carDetailsObj: object = {}
  productTypes = ["Home Insurance", "Car Insurance", "Two Wheelar Insurance", "Health Insurance", "Corona Kavach"]

  NCBValue: string;
  isNCBFlag = false;
  noClaimBonusArr =[{NCBValue:0 },{NCBValue:20},{NCBValue:25},{NCBValue:35},{NCBValue:45 },{NCBValue:50}]
  filterForm = this.formBuilder.group({
    "filterByType": ['All Types', [Validators.required,]],
    'existingPolicyType': '',
    'dosePolicyExpire': '',
    'claimInYourExistingPolicy': 'NO',
    'add_on': "['pa']",
    'ncb': '0%',
    'zeroDepreciation': 'NO',
    'price': "Price (Lowest to High)",
    // edit vehical details
    'varient':"",
    'vehicalType':'Private',
    'registrationDate' : '',
    'ManufacturingDate':'',
    'carFuelType':'',
  })

  /* edit vehical details data */
  public text: string = "Select a game";
  public addOnsArr: { [key: string]: Object }[] = [
    {varientName: 'Engine Protection',varientsm : 'eng',},
    { varientName: 'Lost/ Stoln Keys Cover',varientsm : 'lost',},
    {varientName: 'Road Side Assistance Cover',varientsm : 'rsi',},
    {varientName: 'No Claim Bonus Protection',varientsm : 'ncb',},
    {varientName: 'Return to Invoice',varientsm : 'rti', },
    {varientName: 'Personal Accident Cover',varientsm : 'pa',}
  ];

  public fields: Object = { text: 'varientName', value: 'varientsm' };
  vehicalType :any = 'Private';

/*  */


  public pastDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'mm/dd/yyyy',
    alignSelectorRight: true,
    // disableUntil:
    // disableSince :{year: new Date().getFullYear(),
    //   month: new Date().getMonth() + 1,
    //   day: new Date().getDate()
    // },
  };

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  ngOnInit(): void {
    this.carDetailsObj = (JSON.parse(localStorage.getItem('c_details')) != null && JSON.parse(localStorage.getItem('c_details')) != undefined) ? JSON.parse(localStorage.getItem('c_details')) : {};
    console.log('======>>', this.carDetailsObj);
    if (Object.keys(this.carDetailsObj).length == 0) {
      this.router.navigateByUrl('/four-wheeler-get-details')
    }
    this.setData();

    console.log('====<<<<<<<<',this.varientArr)
    //
    this.dropdownList = [
      { "id": 1, "itemName": "India" },
      { "id": 2, "itemName": "Singapore" },
      { "id": 3, "itemName": "Australia" },
      { "id": 4, "itemName": "Canada" },
      { "id": 5, "itemName": "South Korea" },
      { "id": 6, "itemName": "Germany" },
      { "id": 7, "itemName": "France" },
      { "id": 8, "itemName": "Russia" },
      { "id": 9, "itemName": "Italy" },
      { "id": 10, "itemName": "Sweden" }
    ];
    this.selectedItems = [
      { "id": 2, "itemName": "Singapore" },
      { "id": 3, "itemName": "Australia" },
      { "id": 4, "itemName": "Canada" },
      { "id": 5, "itemName": "South Korea" }
    ];
    this.dropdownSettings = {
      singleSelection: false,
      text: "Select Countries",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
  }
  setData() {
    if (this.carDetailsObj['carRegistrationYear'] != 'Brand new car') {
      let element = document.getElementById('extestingPolicy') as HTMLElement;
      element.click();
    }
    // this.getCheckedItemList();
    let data = {
      option: 'fetch-all-previous-insurance-company'
    }
    this.fetchAllPreviousInsuranceCompany(data);
    this.filterForm.controls['registrationDate'].setValue(this.carDetailsObj['registrationDate']);
    this.filterForm.controls['carFuelType'].setValue(this.carDetailsObj['carFuelType']);
  }


  filterDataByType() {
    let value = this.filterForm.controls['filterByType'].value;
    this.policyList = this.allPolicyList;
    if (value != 'All Types') {
      this.policyList = this.policyList.filter(function (quote) {
        return quote.name === value;
      });
    }

    this.changeilterType()
  }
  doseClaimedPolicy(text) {
    text == '1' ? this.filterForm.controls['claimInYourExistingPolicy'].setValue('YES') : this.filterForm.controls['claimInYourExistingPolicy'].setValue('NO')

    let data = {
      Make: "MARUTI",
      Manufacturer: "2019",
      Model: "BALENO",
      NCB: 0,
      Number: "MH-12-PP-9787",
      PreviousClaim: "0",
      Varient: "1.6 GLX",
      addon: "[↵  'pa'↵]",
      car_end_date: "20/10/2020",
      car_registration_date: "21/10/2019",
      checkboxValuehdfc: true,
      checkboxValuereligare: true,
      checkboxValueshriram: true,
      city: "PUNE",
      name: "fdgdgdf",
      phone: "8208966949",
      policy_insure: "existing_policy",
      previous_insure: "ICICI",

    }
    let data1 = {
      Make: this.carDetailsObj['carBrand'],
      Manufacturer: this.carDetailsObj['carRegistrationYear'],
      Model: this.carDetailsObj['carModel'],
      NCB: this.filterForm.controls['ncb'].value,
      Number: "MH-12-PP-9787",
      PreviousClaim: this.filterForm.controls['claimInYourExistingPolicy'].value,
      Varient: this.carDetailsObj['carVarient'],
      addon: this.filterForm.controls['add_on'].value,
      car_end_date: "20/10/2020",
      car_registration_date: this.carDetailsObj['registrationDate'],
      checkboxValuehdfc: true,
      checkboxValuereligare: true,
      checkboxValueshriram: true,
      city: 'pune',
      name: this.carDetailsObj['clientName'],
      phone: this.carDetailsObj['clientMobile'],
      policy_insure: "existing_policy",
      previous_insure: "ICICI",

    }
    let d = {
      Make: "MARUTI",
      Manufacturer: "2019",
      Model: "BALENO",
      NCB: 0,
      Number: "MH-12-PP-9787",
      PreviousClaim: "0",
      Varient: "1.6 GLX",
      addon: "['pa']",
      car_end_date: "20/10/2020",
      car_registration_date: "21/10/2019",
      checkboxValuehdfc: true,
      checkboxValuereligare: true,
      checkboxValueshriram: true,
      city: "PUNE",
      name: "fdgdgdf",
      phone: "8208966949",
      policy_insure: "existing_policy",
      previous_insure: "ICICI"
    }
    this.getPolicyPlans(data1)
  }

  getPolicyPlans(data) {
    this.fourWheelerService.getAllQuotes(data).subscribe(
      res => {
        console.log("===>>>", res)
      },
      err => {
        console.error("===>", err)
      }
    )
  }

  DateFieldChanged(id1, id2) {
    let element = document.getElementById(id1) as HTMLElement;
    element.click();
    element = document.getElementById(id2) as HTMLElement;
    element.click();
  }

  //
  masterSelected: boolean;
  checkedList: any;
  // masterSelected = false;
  addOnArr = [
    { id: 1, value: 'Zero Depreciation', isSelected: false },
    { id: 2, value: '24x7 Roadside Assistance', isSelected: true },
    { id: 3, value: 'Engine Protection Cover', isSelected: true },
    { id: 4, value: 'NCB Protector', isSelected: false },
    { id: 5, value: 'Key & Lock Replacement', isSelected: false },
    { id: 6, value: 'Consumables', isSelected: false },
    { id: 7, value: 'Daily Allowance', isSelected: false },
    { id: 8, value: 'Invoice Price', isSelected: false },
    { id: 9, value: 'Tyre Protector', isSelected: false },
    { id: 10, value: 'RIM Damage Cover', isSelected: false },
    { id: 11, value: 'Loss of Personal Belongings', isSelected: false },

  ];

  insurerArr = [
    // { id: 1, value: 'Bajaj Allianz', isSelected: false },
    // { id: 2, value: 'Digit General Insurance', isSelected: true },
    // { id: 3, value: 'HDFC ERGO', isSelected: true },
    // { id: 4, value: 'MAGMA HDI', isSelected: false },
    // { id: 5, value: 'New India Assurance', isSelected: false },
    // { id: 6, value: 'Reliance', isSelected: false },
    // {id:7,value:'Daily Allowance',isSelected:false},
    // {id:8,value:'Invoice Price',isSelected:false},
    // {id:9,value:'Tyre Protector',isSelected:false},
    // {id:10,value:'RIM Damage Cover',isSelected:false},
    // {id:11,value:'Loss of Personal Belongings',isSelected:false},

  ]

  fetchAllPreviousInsuranceCompany(data) {
    this.fourWheelerService.getAllCity(data).subscribe(
      res => {
        console.log("===>>>fetchAllPreviousInsuranceCompany", res)

        res['data'].forEach(function (item) {
          item['isSelected'] = true
        });
        this.insurerArr = res['data'];
        console.error('+++++++++++',this.insurerArr)
      },
      err => {
        console.error("===>", err)
      }
    )
  }

  // checkUncheckAll() {
  //   for (var i = 0; i < this.addOnArr.length; i++) {
  //     this.addOnArr[i].isSelected = this.masterSelected;
  //   }
  //   this.getCheckedItemList();
  // }
  // isAllSelected() {
  //   this.masterSelected = this.addOnArr.every(function (item: any) {
  //     return item.isSelected == true;
  //   })
  //   this.getCheckedItemList();
  // }

  // getCheckedItemList() {
  //   this.checkedList = [];
  //   for (var i = 0; i < this.addOnArr.length; i++) {
  //     if (this.addOnArr[i].isSelected)
  //       this.checkedList.push(this.addOnArr[i]);
  //   }
  //   this.checkedList = JSON.stringify(this.checkedList);
  // }

  // InsurarerSelected: boolean = false;
  // checkedInsurerList: any;
  // checkUncheckAllInsurer() {
  //   for (var i = 0; i < this.addOnArr.length; i++) {
  //     this.insurerArr[i].isSelected = this.InsurarerSelected;
  //   }
  //   this.getCheckedItemList();
  // }
  // getCheckedInsurerItemList() {
  //   this.checkedList = [];
  //   for (var i = 0; i < this.insurerArr.length; i++) {
  //     if (this.insurerArr[i].isSelected)
  //       this.checkedList.push(this.insurerArr[i]);
  //   }
  //   this.checkedList = JSON.stringify(this.checkedList);
  // }
  // isAllSelectedInsurer() {
  //   this.InsurarerSelected = this.insurerArr.every(function (item: any) {
  //     return item.isSelected == true;
  //   })
  //   console.log('=================>>>',this.InsurarerSelected,this.insurerArr)
  //   this.getCheckedItemList();
  // }

  // getCheckedInsurerList() {
  //   this.checkedInsurerList = [];
  //   for (var i = 0; i < this.insurerArr.length; i++) {
  //     if (this.insurerArr[i].isSelected)
  //       this.checkedInsurerList.push(this.addOnArr[i]);
  //   }
  //   this.checkedInsurerList = JSON.stringify(this.checkedInsurerList);
  // }

  onItemSelect(item: any) {
    console.log(item);
    console.log(this.selectedItems);
  }
  OnItemDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedItems);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }

  changeilterType() {
    // text == '1' ? this.filterForm.controls['claimInYourExistingPolicy'].setValue('YES') : this.filterForm.controls['claimInYourExistingPolicy'].setValue('NO')

    let data = {
      Make: "MARUTI",
      Manufacturer: "2019",
      Model: "BALENO",
      NCB: 0,
      Number: "MH-12-PP-9787",
      PreviousClaim: "0",
      Varient: "1.6 GLX",
      addon: "['pa','0']",
      car_end_date: "20/10/2020",
      car_registration_date: "21/10/2019",
      checkboxValuehdfc: true,
      checkboxValuereligare: true,
      checkboxValueshriram: true,
      city: "PUNE",
      name: "fdgdgdf",
      phone: "8208966949",
      policy_insure: "existing_policy",
      previous_insure: "ICICI",

    }
    let data1 = {
      Make: this.carDetailsObj['carBrand'],
      Manufacturer: this.carDetailsObj['carRegistrationYear'],
      Model: this.carDetailsObj['carModel'],
      NCB: this.filterForm.controls['ncb'].value,
      Number: "MH-12-PP-9787",
      PreviousClaim: this.filterForm.controls['claimInYourExistingPolicy'].value == "YES" ? 1 : 0,
      Varient: this.carDetailsObj['carVarient'],
      addon: this.filterForm.controls['add_on'].value,
      // addon: "['pa']",
      car_end_date: "20/10/2020",
      car_registration_date: this.carDetailsObj['registrationDate'],
      checkboxValuehdfc: true,
      checkboxValuereligare: true,
      checkboxValueshriram: true,
      city: this.carDetailsObj['carCity'],
      name: this.carDetailsObj['clientName'],
      phone: this.carDetailsObj['clientMobile'],
      policy_insure: "existing_policy",
      previous_insure: "ICICI",

    }

    let d = {
      Make: "MARUTI",
      Manufacturer: "2019",
      Model: "BALENO",
      NCB: 0,
      Number: "MH-12-PP-9787",
      PreviousClaim: "0",
      Varient: "1.6 GLX",
      addon: "['pa']",
      car_end_date: "20/10/2020",
      car_registration_date: "21/10/2019",
      checkboxValuehdfc: true,
      checkboxValuereligare: true,
      checkboxValueshriram: true,
      city: "PUNE",
      name: "fdgdgdf",
      phone: "8208966949",
      policy_insure: "existing_policy",
      previous_insure: "ICICI"
    }
    this.getPolicyPlans(d)
  }

  saveEditedVhehicalDetails(){
    console.error(this.vehicalType,this.filterForm)
  }

  /* */

  insurerToggle:boolean = true;
  selectDeselectAll(evt){
    console.log(evt);
    if(evt.target.checked){
     this.insurerArr.forEach(function (data) {
       data['isSelected'] = true;
     })
    }else{
     this.insurerArr.forEach(function (data) {
       data['isSelected'] = false;
     })
    }
  }



  selectDeselectInsurer(evt,ind){
   console.log(evt);
     if(evt.target.checked){
       this.insurerArr[ind]['isSelected'] = true;
     }else{
       this.insurerArr[ind]['isSelected'] = false;
     }

     this.checkAllSelected()
   }

   checkAllSelected(){
     let isSelectedFlag = false;
     this.insurerArr.forEach(function (data) {
      if( data['isSelected'] == false){
       isSelectedFlag = true
      }
     })

     if(isSelectedFlag){
       this.insurerToggle = false;
     }else{
       this.insurerToggle = true
     }
   }

   /* */
   non_electrical_acc=false;
   electrical_acc=false;
   isPassangerCover=false;
   isDrivercover=false;
   passangerSumInsured =[{pcoverValue:10000 },{pcoverValue:50000},{pcoverValue:100000},{pcoverValue:200000}]
   driverSumInsured =[{'dcoverValue':10000 },{'dcoverValue':50000},{'dcoverValue':100000},{'dcoverValue':200000}]

   addonsArr =[{addonKey: "eng",addonValue:"Engine Protection",isSelected:false},{addonKey: "lost",addonValue:"Lost/ Stoln Keys Cover",isSelected:false}
   ,{addonKey: "rsi",addonValue:"Road Side Assistance Cover",isSelected:false},{addonKey: "ncb",addonValue:"No Claim Bonus Protection",isSelected:false}
   ,{addonKey: "rti",addonValue:"Return to Invoice",isSelected:false },{addonKey: "pa",addonValue:"Personal Accident Cover" ,isSelected:false}]

   checkUncheckAddons(event,ind)
   {
     console.log(event);
     if(event.target.checked){
       this.addonsArr[ind]['isSelected'] = true
     }else{
       this.addonsArr[ind]['isSelected'] = false
     }
   }

   clearAddons()
   {
      this.addonsArr.forEach(data => {
       data['isSelected'] = false;
     });
     console.log("=====>>>",this.addonsArr)
     let temp = this.addonsArr
     this.addonsArr =[]
     this.addonsArr = temp
   }

   setAddonsAndAccessories(){
     console.log("inside setAddonsAndAccessories")
     let arr = [];
     for(let i in this.addonsArr){
       if(this.addonsArr[i]['isSelected']){
         arr.push(this.addonsArr[i]['addonKey'])
       }
     }
     this.filterForm.controls['add_on'].setValue(arr)
     console.log("===______this.filterForm.controls['add_on']",this.filterForm.controls['add_on'].value);

     this.changeilterType()
   }


}
