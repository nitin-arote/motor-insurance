import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule} from "@angular/forms";
import { FourWheelerRoutingModule } from "./four-wheeler.routing";
import { FourWheelerGetquoteComponent } from "./four-wheeler-getquote/four-wheeler-getquote.component";
import { FourWheelerComponent } from "./four-wheeler/four-wheeler.component";
import { FourWheelerPlansComponent } from "./four-wheeler-plans/four-wheeler-plans.component";
import { FourWheelerGetDetailsComponent } from "./four-wheeler-get-details/four-wheeler-get-details.component";
import { FourWheelerService } from "./four-wheeler.service";
@NgModule({
  declarations: [FourWheelerGetquoteComponent,FourWheelerComponent,FourWheelerPlansComponent,FourWheelerGetDetailsComponent],
  imports: [
    CommonModule,FourWheelerRoutingModule,FormsModule,ReactiveFormsModule
  ],
  providers:[FourWheelerService]
})
export class FourWheelerModule { }
