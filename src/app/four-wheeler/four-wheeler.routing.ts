import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FourWheelerGetquoteComponent } from "./four-wheeler-getquote/four-wheeler-getquote.component";
import { FourWheelerComponent } from "./four-wheeler/four-wheeler.component";
import { FourWheelerPlansComponent } from "./four-wheeler-plans/four-wheeler-plans.component";
import { FourWheelerGetDetailsComponent } from "./four-wheeler-get-details/four-wheeler-get-details.component";

const routes: Routes = [
    {
      path: '',
      component: FourWheelerGetquoteComponent,
      redirectTo:'four-wheeler-getquote',
    },
    {
      path: "four-wheeler-getquote",
      component: FourWheelerGetquoteComponent
    },
    {
      path: 'four-wheeler-get-details',
      component: FourWheelerGetDetailsComponent
    },
    {
      path: 'four-wheeler-plans',
      component: FourWheelerPlansComponent
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FourWheelerRoutingModule { }
