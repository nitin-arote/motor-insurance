import { TestBed } from '@angular/core/testing';

import { FourWheelerService } from './four-wheeler.service';

describe('FourWheelerService', () => {
  let service: FourWheelerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FourWheelerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
