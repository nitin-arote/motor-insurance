import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetMotorDeatilsComponent } from './get-motor-deatils.component';

describe('GetMotorDeatilsComponent', () => {
  let component: GetMotorDeatilsComponent;
  let fixture: ComponentFixture<GetMotorDeatilsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetMotorDeatilsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetMotorDeatilsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
