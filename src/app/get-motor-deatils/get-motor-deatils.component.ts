import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormControl, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { TwoWheelerService } from '../two-wheeler/two-wheeler.service';
// import { CommonService } from "../../shared/common.service";
@Component({
  selector: 'app-get-motor-deatils',
  templateUrl: './get-motor-deatils.component.html',
  styleUrls: ['./get-motor-deatils.component.css']
})
export class GetMotorDeatilsComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private twoWheelerService: TwoWheelerService) { }

  ngOnInit(): void {
    this.getAllCity();
    this.getAllManufacturer();
    this.addYear();
    // let element = document.getElementById('field-1') as HTMLInputElement;
    // element.click();

  }



  bikeYear = [];
  quoteForm = this.formBuilder.group({
    "city": "",
    "manufacturer": "",
    "model": "",
    "varient" :"",
    "registrationYear": ""
  })

  addYear() {
    for (let i = 2020; i >= 2000; i--) {
      this.bikeYear.push({ 'year': i })
    }
  }
  getDetailsMode = 1;

  cityArr: any = [];
  cityArrBk: any = [];
  manufacturerArr:any = [];
  manufacturerArrBk:any = [];
  bikeModalsArr:any = [];
  bikeModalsArrBk:any = [];
  bikeVarientArr : any = [];
  bikeVarientArrBk : any = []
  isLoading: boolean = true;
  getAllCity() {
    let data = {
      option: 'fetch-all-rto-city'
    }
    this.twoWheelerService.getAllCity(data).subscribe(
      response => {
        console.log("fetch-all-rto-city ===>>>>", response['data']);
        this.cityArr = response['data'];
        this.cityArrBk = response['data'];
        this.twoWheelerService.allCity = response['data'];
      },
      error => {
        console.error('==', error)
      },
      () => {
        this.isLoading = false
      }
    );
  }

    /* Get all city */
    getAllManufacturer() {
      // this.isLoading = true;
      let data = {
        option: 'fetch-all-distinct-manufacturer'
      }
      this.twoWheelerService.getAllCity(data).subscribe(
        response => {
          console.log("fetch-all-distinct-manufacturer ====>>>>", response['data']);
          this.manufacturerArr = response['data'];
          this.manufacturerArrBk = response['data'];
          this.twoWheelerService.bikeManufacturer =  response['data'];
        },
        error => {
          console.error('==', error)
        },
      );
    }

    getAllModels(manufacturer) {
      this.isLoading = true;
      let data = {
        option: 'fetch-all-distinct-model',
        // modelcode:'',
        manufacturercode: manufacturer
      }
      this.twoWheelerService.getAllCity(data).subscribe(
        response => {
          console.log("fetch-all-distinct-model ====>>>>", response['data']);
          this.bikeModalsArr = response['data'];
          this.bikeModalsArrBk = response['data'];
          this.twoWheelerService.bikeModel =  response['data'];
        },
        error => {
          console.error('==', error)
        },
        () => {
          this.isLoading = false
        }
      );
    }

  getAllVarient(manufacturer, model) {
    this.isLoading = true;
    let data = {
      option: 'fetch-all-distinct-varient',
      manufacturercode: manufacturer,
      modelcode: model
    }
    this.twoWheelerService.getAllCity(data).subscribe(
      response => {
        console.log("fetch-all-distinct-varient ====>>>>", response);
        this.bikeVarientArr = response['data'];
        this.bikeVarientArrBk = response['data'];
        this.twoWheelerService.bikeVariant =  response['data'];
      },
      error => {
        console.error('==', error)
      },
      () => {
        this.isLoading = false
      }
    );
  }

}
