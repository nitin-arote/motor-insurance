import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor() { }
  isNumbers(evt) { // To enter numbers only (without decimal)
    var charCode = (evt.which) ? evt.which : event['keyCode']
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }
  isText(evt) { // To enter numbers only text
    var charCode = (evt.which) ? evt.which : event['keyCode']
    if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122) && (charCode != 32))
      return false;
    return true;
  }
  getIdFromObject(obj) {
    if (obj != null || obj != undefined)
      return obj['id']
    else
      return obj
  }
  formatToMotorNumber = (event) => {
    // if (this.isModifierKey(event)) { return; }
    const target = event.target;
    const input = event.target.value.trim().substring(0, 15); // First ten digits of input only
    const zip = input.substring(0, 2);
    const zip1 = input.substring(3, 5);
    const zip2 = input.substring(6, 8);
    const zip3 = input.substring(9, 13);
    // const last = input.substring(8, 10);
    // console.log("keycode",event.keyCode )
    if(event.keyCode != 8){
      // console.log( event.target.value )
      if (input.length > 7) {
      //  console.log('zip3',zip3);
       target.value = `${zip}-${zip1}-${zip2}-${zip3}`; 
     }
     else if (input.length > 4) {
      //  console.log('zip2',zip2);
       target.value = `${zip}-${zip1}-${zip2}`; 
     }
     else if (input.length > 2) {
      //  console.log('zip1',zip1);
       target.value = `${zip}-${zip1}`; 
     }else if (input.length > 1) {
      //  console.log('zip',zip);
        target.value = `${zip}-`; 
     }
    }

 
    
  };

  // const zip = input.substring(0, 3);
  // const middle = input.substring(3, 6);
  // const last = input.substring(6, 10);
  // else if (input.length > 2) { target.value = `(${zip}) ${middle}`; }
  //   else if (input.length > 0) { target.value = `(${zip}`; }
  // validateNumberPlate(event){
  filterArrayBetweenDateRange(startDate,endDate,data){  
      var forDate = new Date(startDate).getTime();
      var toDate =  new Date(endDate).getTime();
      let filteredData = []
      for(let quote of data){
        var dateA = new Date(quote.date).getTime()
        if((forDate <= dateA) && ((toDate >= dateA))){
          console.log(quote)
          filteredData.push(quote)
        }
      }
      return filteredData;
    }; 
}
