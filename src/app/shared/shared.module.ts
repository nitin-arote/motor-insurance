import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonService } from "./common.service";
import { AlertOnRightComponent } from './alert-on-right/alert-on-right.component';


@NgModule({
  declarations: [AlertOnRightComponent],
  imports: [
    CommonModule
  ],
  exports:[AlertOnRightComponent]
})
export class SharedModule { }
