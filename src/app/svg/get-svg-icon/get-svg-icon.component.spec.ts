import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetSvgIconComponent } from './get-svg-icon.component';

describe('GetSvgIconComponent', () => {
  let component: GetSvgIconComponent;
  let fixture: ComponentFixture<GetSvgIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetSvgIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetSvgIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
