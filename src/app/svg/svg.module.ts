import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetSvgIconComponent } from './get-svg-icon/get-svg-icon.component';



@NgModule({
  declarations: [GetSvgIconComponent],
  imports: [
    CommonModule
  ],
  exports:[GetSvgIconComponent]
})
export class SvgModule { }
