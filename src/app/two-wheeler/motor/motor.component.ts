import { Component, OnInit } from '@angular/core';
import { TwoWheelerService } from "../two-wheeler.service";
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-motor',
  templateUrl: './motor.component.html',
  styleUrls: ['./motor.component.css']
})
export class MotorComponent implements OnInit {

  
  bikeNumber : any= ''
  
  constructor(private twoWheelerService : TwoWheelerService,public commonService : CommonService){}
  ngOnInit(){ }
    
    getQuote(){
      console.log(this.bikeNumber)
      let data = this.bikeNumber;
      data = data.replace(/-/g,'');
      this.twoWheelerService.getBikeDetails(data).subscribe(
        res=>{
          console.log(res);
        },
        err=>{
          console.error(err)
        }
      )
    }
}
