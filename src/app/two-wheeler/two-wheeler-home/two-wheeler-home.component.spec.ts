import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoWheelerHomeComponent } from './two-wheeler-home.component';

describe('TwoWheelerHomeComponent', () => {
  let component: TwoWheelerHomeComponent;
  let fixture: ComponentFixture<TwoWheelerHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwoWheelerHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoWheelerHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
