import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormControl, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { CommonService } from "../../shared/common.service";
import { TwoWheelerService } from "../two-wheeler.service";
@Component({
  selector: 'app-two-wheeler-home',
  templateUrl: './two-wheeler-home.component.html',
  styleUrls: ['./two-wheeler-home.component.css']
})
export class TwoWheelerHomeComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private twoWheelerService: TwoWheelerService) { }

  ngOnInit(): void {
    this.getAllCity();
    this.getAllManufacturer();
    this.addYear();
    let element = document.getElementById('field-1') as HTMLInputElement;
    // element.addEventListener('click',)
    element.click();

  }

  city = []




  bikeYear = [];


  quoteForm = this.formBuilder.group({
    "city": "",
    "manufacturer": "",
    "model": "",
    "varient" :"",
    "registrationYear": ""
  })

  addYear() {
    for (let i = 2020; i >= 2000; i--) {
      this.bikeYear.push({ 'year': i })
    }
  }

  bikeCity = '';
  bikeManufacturer = '';
  bikeModel = '';
  bikeVarient = '';
  bikeRegistrationYear = ''


  addCity(data, index) {
    console.log('====================>>>', data)
    this.quoteForm.controls['city'].setValue(data['NUM_REGISTRATION_CODE']);
    this.bikeCity = data['NUM_REGISTRATION_CODE'];
    let element = document.getElementById('field-2') as HTMLInputElement;
    // element.addEventListener('click',)
    element.click();
  }

  addManufacturer(data, index) {
    console.log('====================>>>', data)
    this.quoteForm.controls['manufacturer'].setValue(data['MANUFACTURER'])
    this.bikeManufacturer = data['MANUFACTURER'];
    this.getAllModels(data['MANUFACTURER']);
    let element = document.getElementById('field-3') as HTMLInputElement;
    // element.addEventListener('click',)
    element.click();
  }

  addBikeModals(data, index) {
    console.log('====================>>>', data)
    this.quoteForm.controls['model'].setValue(data['MODEL'])
    this.bikeModel = data['MODEL'];
    this.getAllVarient(this.bikeManufacturer, data['MODEL'])
    let element = document.getElementById('field-5') as HTMLInputElement;
    // element.addEventListener('click',)
    element.click();
  }
  addBikeVarient(data, index) {
    console.log('====================>>>', data)
    this.quoteForm.controls['varient'].setValue(data['VARIANT'])
    this.bikeVarient = data['VARIANT'];
    let element = document.getElementById('field-4') as HTMLInputElement;
    element.click();
  }

  addBikeYear(data, index) {
    console.log('====================>>>', data)
    this.quoteForm.controls['registrationYear'].setValue(data['year']);
    this.bikeRegistrationYear = data['year'];
    let element = document.getElementById('selectCityRTOClose') as HTMLElement;
    element.click();
  }

  //  All Dynamic Data
  /* Get all city */
  cityArr: any = [];
  cityArrBk: any = [];
  manufacturerArr:any = [];
  manufacturerArrBk:any = [];
  bikeModalsArr:any = [];
  bikeModalsArrBk:any = [];
  bikeVarientArr : any = [];
  bikeVarientArrBk : any = []
  isLoading: boolean = true;
  getAllCity() {
    let data = {
      option: 'fetch-all-rto-city'
    }
    this.twoWheelerService.getAllCity(data).subscribe(
      response => {
        console.log("fetch-all-rto-city ===>>>>", response['data']);
        this.cityArr = response['data'];
        this.cityArrBk = response['data'];
        this.twoWheelerService.allCity = response['data'];
      },
      error => {
        console.error('==', error)
      },
      () => {
        this.isLoading = false
      }
    );
  }

  /* Get all city */
  getAllManufacturer() {
    // this.isLoading = true;
    let data = {
      option: 'fetch-all-distinct-manufacturer'
    }
    this.twoWheelerService.getAllCity(data).subscribe(
      response => {
        console.log("fetch-all-distinct-manufacturer ====>>>>", response['data']);
        this.manufacturerArr = response['data'];
        this.manufacturerArrBk = response['data'];
        this.twoWheelerService.bikeManufacturer =  response['data'];
      },
      error => {
        console.error('==', error)
      },
      () => {
        // this.isLoading = false
      }
    );
  }

  getAllModels(manufacturer) {
    this.isLoading = true;
    let data = {
      option: 'fetch-all-distinct-model',
      // modelcode:'',
      manufacturercode: manufacturer
    }
    this.twoWheelerService.getAllCity(data).subscribe(
      response => {
        console.log("fetch-all-distinct-model ====>>>>", response['data']);
        this.bikeModalsArr = response['data'];
        this.bikeModalsArrBk = response['data'];
        this.twoWheelerService.bikeModel =  response['data'];
      },
      error => {
        console.error('==', error)
      },
      () => {
        this.isLoading = false
      }
    );
  }


  getAllVarient(manufacturer, model) {
    this.isLoading = true;
    let data = {
      option: 'fetch-all-distinct-varient',
      manufacturercode: manufacturer,
      modelcode: model
    }
    this.twoWheelerService.getAllCity(data).subscribe(
      response => {
        console.log("fetch-all-distinct-varient ====>>>>", response);
        this.bikeVarientArr = response['data'];
        this.bikeVarientArrBk = response['data'];
        this.twoWheelerService.bikeVariant =  response['data'];
      },
      error => {
        console.error('==', error)
      },
      () => {
        this.isLoading = false
      }
    );
  }

  setInputValues(){
    localStorage.setItem('userdata',JSON.stringify(this.quoteForm.value))
  }
}
