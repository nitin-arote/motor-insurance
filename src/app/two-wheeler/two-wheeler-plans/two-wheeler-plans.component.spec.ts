import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoWheelerPlansComponent } from './two-wheeler-plans.component';

describe('TwoWheelerPlansComponent', () => {
  let component: TwoWheelerPlansComponent;
  let fixture: ComponentFixture<TwoWheelerPlansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwoWheelerPlansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoWheelerPlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
