import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-two-wheeler-plans',
  templateUrl: './two-wheeler-plans.component.html',
  styleUrls: ['./two-wheeler-plans.component.css']
})
export class TwoWheelerPlansComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,) { }

  ngOnInit(): void {
  }

  insurerToggle:boolean = true;
  policyList = [
    {policy_id:"1",name:"Home Insurance",description : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",start_date:"20/08/2020" ,end_date:"20/08/2020"},
    {policy_id:"2",name:"Car Insurance",description : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",start_date:"20/08/2020" ,end_date:"20/08/2020"},
    {policy_id:"3",name:"Two Wheelar Insurance",description : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",start_date:"20/08/2020" ,end_date:"20/08/2020"},
    {policy_id:"4",name:"Health Insurance",description : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",start_date:"20/08/2020" ,end_date:"20/08/2020"},
    {policy_id:"5",name:"Health Insurance",description : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",start_date:"20/08/2020" ,end_date:"20/08/2020"},
    {policy_id:"6",name:"Corona Kavach",description : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",start_date:"20/08/2020" ,end_date:"20/08/2020"},
    {policy_id:"7",name:"Home Insurance",description : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",start_date:"20/08/2020" ,end_date:"20/08/2020"},
    {policy_id:"8",name:"Home Insurance",description : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",start_date:"20/08/2020" ,end_date:"20/08/2020"}
   ]
   insrerArr = [
    {policy_id:"1",name:"Home Insurance",iselected:true},
    {policy_id:"2",name:"Car Insurance",iselected:true},
    {policy_id:"3",name:"Two Wheelar Insurance",iselected:true},
    {policy_id:"4",name:"Health Insurance",iselected:true},
    {policy_id:"5",name:"Health Insurance",iselected:true},
    {policy_id:"6",name:"Corona Kavach",iselected:true},
    {policy_id:"7",name:"Home Insurance",iselected:true},
    {policy_id:"8",name:"Home Insurance",iselected:true}
   ]
   allPolicyList = this.policyList
   productTypes = ["Home Insurance","Car Insurance","Two Wheelar Insurance","Health Insurance","Corona Kavach"]
   filterForm = this.formBuilder.group({
    "filterByType" : ['All Types',[Validators.required,]],
  })
   filterDataByType(){
    let value = this.filterForm.controls['filterByType'].value;
    this.policyList = this.allPolicyList;
    if(value != 'All Types'){
      this.policyList = this.policyList.filter(function (quote) {
        return quote.name ===  value;
      });
    }
 }

 selectDeselectAll(evt){
   console.log(evt);
   if(evt.target.checked){
    this.insrerArr.forEach(function (data) {
      data['iselected'] = true;
    })
   }else{
    this.insrerArr.forEach(function (data) {
      data['iselected'] = false;
    })
   }
 }

 selectDeselectInsurer(evt,ind){
  console.log(evt);
    if(evt.target.checked){
      this.insrerArr[ind]['iselected'] = true;
    }else{
      this.insrerArr[ind]['iselected'] = false;
    }

    this.checkAllSelected()
  }

  checkAllSelected(){
    let isSelectedFlag = false;
    this.insrerArr.forEach(function (data) {
     if( data['iselected'] == false){
      isSelectedFlag = true
     }
    })

    if(isSelectedFlag){
      this.insurerToggle = false;
    }else{
      this.insurerToggle = true
    }
  }
}
