import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TwoWheelerRoutingModule } from "./two-wheeler.routing";
import { FormsModule,ReactiveFormsModule} from "@angular/forms";
import { TwoWheelerPlansComponent } from "./two-wheeler-plans/two-wheeler-plans.component";
import { TwoWheelerHomeComponent } from "./two-wheeler-home/two-wheeler-home.component";
import { TwoWheelerComponent } from "./two-wheeler/two-wheeler.component";
import { MotorComponent } from "./motor/motor.component";

@NgModule({
  declarations: [MotorComponent,TwoWheelerPlansComponent,TwoWheelerHomeComponent,TwoWheelerComponent],
  imports: [
    CommonModule,TwoWheelerRoutingModule,FormsModule,ReactiveFormsModule
  ]
})
export class TwoWheelerModule { }
