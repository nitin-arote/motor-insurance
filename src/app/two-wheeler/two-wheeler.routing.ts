import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MotorComponent } from "./motor/motor.component";
import { TwoWheelerHomeComponent } from "./two-wheeler-home/two-wheeler-home.component";
import { TwoWheelerComponent } from "./two-wheeler/two-wheeler.component";
import { TwoWheelerPlansComponent } from "./two-wheeler-plans/two-wheeler-plans.component";
const routes: Routes = [
    {
      path: '',
      redirectTo:'two-wheeler-getquote',
      pathMatch:'full'
    },
    {
      path: "two-wheeler-getquote",
      component: MotorComponent
    },
    {
      path: 'two-wheeler-get-details',
      component: TwoWheelerHomeComponent
    },
    {
      path: 'two-wheeler-plans',
      component: TwoWheelerPlansComponent
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TwoWheelerRoutingModule { }
