import { TestBed } from '@angular/core/testing';

import { TwoWheelerService } from './two-wheeler.service';

describe('TwoWheelerService', () => {
  let service: TwoWheelerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TwoWheelerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
