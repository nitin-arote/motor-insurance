import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class TwoWheelerService {

  constructor(private http : HttpClient) { }
  
  allCity : any = [];
  bikeManufacturer : any = []
  bikeVariant : any = []
  bikeModel : any = []
  // getAllCity(data){

 
  //   let headers = new HttpHeaders()
  //   headers .set('content-type', 'application/json')
  //   headers .set('Access-Control-Allow-Origin', '*')
  //   console.log(headers); 
 
  //   return this.http.post("https://www.policyplanner.com/motor-insurance/ws/api.php",data,{ 'headers': headers })
  // }
  getAllCity(data){
      return this.http.post("https://www.policyplanner.com/motor-insurance/ws/api.php",JSON.stringify(data))
  }
  getBikeDetails(motorNo){
    return this.http.get(`https://www.rto.myidfy.com/details/${motorNo}/`)
  }

  // getRecentdata(){
  //   let data = {'option':'fetch-all-financial-blogs'}
  //   return this.http.post("https://policyplanner.com/IJ-Services/IJ-API/fetchAllBlogTest.php",JSON.stringify(data))
  // }
}