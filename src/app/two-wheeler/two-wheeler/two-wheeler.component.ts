import { Component, OnInit } from '@angular/core';
import { FormBuilder } from "@angular/forms";
@Component({
  selector: 'app-two-wheeler',
  templateUrl: './two-wheeler.component.html',
  styleUrls: ['./two-wheeler.component.css']
})
export class TwoWheelerComponent implements OnInit {

  constructor(private formBuilder : FormBuilder) { }

  ngOnInit(): void {
  }

  quoteForm = this.formBuilder.group({
    "task":"",
    "taskDate": ""
   })

}
